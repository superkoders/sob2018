var config = require('./helpers/getConfig.js');

var gulp = require('gulp');
var styleInject = require('gulp-style-inject');

gulp.task('inline-css', function() {
	var stream = gulp.src([
		'*.html',
	], {
		cwd: config.dest.templates,
	});

	stream
		.pipe(styleInject())
		.pipe(gulp.dest(config.dest.templates));

	return stream;
});
