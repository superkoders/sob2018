import $ from 'jquery';
import './tools/svg4everybody';
// import { MQ } from './tools/MQ';

// Components
// import * as fontFaceObserver from './components/fontFaceObserver';


const components = [
	// fontFaceObserver,
];

window.App = {
	run() {
		// MQ('desktopMin');

		components.forEach(this.initComponent);
	},



	initComponent(component) {
		return component.init();
	},
};
